
const headers={
  es: "|L|M|X|J|V|S|D",
  en: "|Mon|Tue|Wed|Thu|Fri|Sat|Sun",
}

function createCalendarBlock(){
    var self = this
    self.onContext('pass')
    self.process((parent, reader, attrs) => {
      var planning = [];
      var lastDay=-1;
      var lines = reader.getLines().forEach(element => {
        if( element.trim().length == 0 ){
          return
        }

        var day = element.split(" ")[0]
        if( !isNaN(day)){
          const nDay = parseInt(day)
          var plan = element.substring(element.indexOf(' ')+1)
          if( !planning[nDay] ){
            planning[nDay] = []
          }
          planning[nDay].push(plan)
          lastDay=nDay
        }else{
          planning[lastDay].push(element)
        }
      });
      
      var date = new Date();
      date.setFullYear(attrs.year)
      date.setMonth(attrs.month-1)
      date.setDate(1)
      var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);  
      date = new Date(date.setDate(diff)); // set to monday

      const header = headers[attrs.lang] || headers['es']

      const oneTable = !(""+attrs.separateWeeks==='true')
      
      var nodes = [
        "."+attrs.year+"/"+attrs.month,
      ]

      const tmp = [
        "[cols=7*,options=header,"+(attrs.extra||"")+"]",
        "|===",
        header,
        ""
      ]      

      nodes = nodes.concat(tmp);

      var dayOfWeek=0;
      while( date.getMonth() < attrs.month-1){
        nodes.push("|")
        date.setDate(date.getDate()+1);
        dayOfWeek++
      }

      while( date.getMonth() < attrs.month){
        nodes.push("a|"+date.getDate())
        if( planning[date.getDate()]){          
          planning[date.getDate()].forEach( element=>{
            nodes.push("")
            nodes.push(element)
            nodes.push("")
          })

        }
        date.setDate(date.getDate()+1);
        if( !oneTable){
          if(++dayOfWeek > 6){
            dayOfWeek=0
            nodes = nodes.concat(["|===","",""])
            nodes = nodes.concat(tmp)
          }
        }         
      }

      while( date.getDate() < date.getDay()+6){
        nodes.push("|")
        date.setDate(date.getDate()+1);
      }
      
      nodes.push("|===");      
      
      return self.createBlock(parent, 'open', nodes)
    });    
  
}

module.exports.register = function (registry, context) {
    registry.block('calendar', createCalendarBlock)
  }