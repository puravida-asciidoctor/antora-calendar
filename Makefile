PWD ?= pwd_unknown
PROJECT_NAME = $(notdir $(PWD))
SERVICE_TARGET := main

ifeq ($(user),)
	# USER retrieved from env, UID from shell.
	HOST_USER ?= $(strip $(if $(USER),$(USER),nodummy))
	HOST_UID ?= $(strip $(if $(shell id -u),$(shell id -u),4000))
else
	# allow override by adding user= and/ or uid=  (lowercase!).
	# uid= defaults to 0 if user= set (i.e. root).
	HOST_USER = $(user)
	HOST_UID = $(strip $(if $(uid),$(uid),0))
endif

THIS_FILE := $(lastword $(MAKEFILE_LIST))
CMD_ARGUMENTS ?= $(cmd)

export PROJECT_NAME
export HOST_USER
export HOST_UID
export S3_BUCKET

.PHONY: help dbuild dpackage dtest dintegrationtest dcheck dclean dremove

help:
	@echo ''
	@echo 'Usage: make [TARGET] [EXTRA_ARGUMENTS]'
	@echo 'Targets:'
	@echo '  dbuild              [Docker] Generates tymit private site'
	@echo '  dlocal              [Docker] Generates documentation using local playground'
	@echo '  dremove             [Docker] Remove containers'
	@echo ''
	@echo 'Extra arguments:'
	@echo 'cmd=:    make cmd="whoami"'
	@echo '# user= and uid= allows to override current user. Might require additional privileges.'

dbuild:
	docker-compose run antora --generator antora-site-generator-lunr local.yml

dremove:
	docker-compose down || true
	docker rm $(shell docker ps -qa --no-trunc --filter "status=exited") || true

dclean:
	docker run -v ${PWD}:/app/workdir alpine:3 \
            rm -rf /app/workdir/build

