# Antora Calendar

Antora extension to generate calendars 


Once installed the extension you can write into your document as showed into the example.adoc


and the extension will convert it to a table with the days for the year/month similar to
https://puravida-asciidoctor.gitlab.io/asciidoctor-extensions/#_calendar


